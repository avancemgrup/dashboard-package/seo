## Avannubo package seo

Seo

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/seo": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "https://gitlab.com/avancemgrup/dashboard-package/seo.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Seo\SeoServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\Seo\Seeds\PermissionSeoSeeder`

<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {
    Route::get('seos', [
        'as' => 'seos',
        'uses' => '\Avannubo\Seo\Controllers\SeoController@index',
        'middleware' => ['permission:seo.view|seo.create|seo.edit|seo.delete']
    ]);
    Route::get('seos/add', [
        'as' => 'seos-add-form',
        'uses' => '\Avannubo\Seo\Controllers\SeoController@create',
        'middleware' => ['permission:seo.create']
    ]);
    Route::get('seos/edit/{id}', [
        'as' => 'seos-edit',
        'uses' => '\Avannubo\Seo\Controllers\SeoController@edit',
        'middleware' => ['permission:seo.edit']
    ]);
    Route::post('seos/add', [
        'as' => 'seos-add',
        'uses' => '\Avannubo\Seo\Controllers\SeoController@store',
        'middleware' => ['permission:seo.create']
    ]);
    Route::put('seos/{id}', [
        'as' => 'seos-edit',
        'uses' => '\Avannubo\Seo\Controllers\SeoController@update',
        'middleware' => ['permission:seo.edit']
    ]);
    Route::delete('seos/{id}', [
        'as' => 'seos-delete',
        'uses' => '\Avannubo\Seo\Controllers\SeoController@delete',
        'middleware' => ['permission:seo.delete']
    ]);

});
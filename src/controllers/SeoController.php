<?php

namespace Avannubo\Seo\Controllers;


use Avannubo\Seo\Models\Seo;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;

class SeoController extends Controller{
    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show the index view
     */
    public function  index(Request $request){
        $search = $request->input('search');
        $seos = Seo::where('title','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('seo::index', compact('seos'));
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method show the form of add new slider
     */
    public function create(){
        return view('seo::form');
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it inserts it into the database
     * else Shows error
     *
     */
    public function store(Request $request){
        // Validation
        $rules = [
            'title' => 'nullable|Max:255',
            'author' => 'nullable|Max:255',
            'description' => 'nullable|Max:400',
            'keyword' => 'nullable|Max:255',
            'robots' => 'nullable|Max:255',
            'canonical_url' => 'nullable|url|Max:255',
            'language' => 'nullable|Max:255',
            'url' => 'required|url|unique:seos,route|Max:255',
            'og_title' => 'nullable|Max:255',
            'og_description' => 'nullable|Max:400',
            'og_type' => 'nullable|Max:255',
            'og_image' => 'nullable|image',
            'og_url' => 'nullable|url|Max:255',
            'og_site_name' => 'nullable|Max:255',
            'tw_card' => 'nullable|Max:255',
            'tw_url' => 'nullable|url|Max:255',
            'tw_title' => 'nullable|Max:255',
            'tw_description' => 'nullable|Max:400',
            'tw_image' => 'nullable|image',
            'gl_page_type' => 'nullable|Max:255',
            'gl_name' => 'nullable|Max:255',
            'gl_description' => 'nullable|Max:400',
            'gl_image' => 'nullable|image',
        ];
        $this->validate($request, $rules);

        if($request->file('og_image') != null) {
            if ($request->file('og_image')) {
                $og_image = $request->file('og_image');
                $og_imageName = 'og' . time() . '.' . $og_image->getClientOriginalExtension();
                Storage::putFileAs('public/seos', new File($og_image), $og_imageName);
            }
        }else{
            $og_imageName = null;
        }

        if($request->file('tw_image') != null) {
            if( $request->file('tw_image')){
                $tw_image = $request->file('tw_image');
                $tw_imageName = 'tw'.time(). '.' .$tw_image->getClientOriginalExtension();
                Storage::putFileAs('public/seos', new File($tw_image), $tw_imageName );

            }
        }else{
            $tw_imageName =  null;
        }

        if($request->file('gl_image') != null) {
            if( $request->file('gl_image')){
                $gl_image = $request->file('gl_image');
                $gl_imageName = 'gl'.time(). '.' .$gl_image->getClientOriginalExtension();
                Storage::putFileAs('public/seos', new File($gl_image), $gl_imageName );
            }
        }else{
            $gl_imageName = null;
        }
        $seos = new Seo([
            'title' => $request->input('title'),
            'author' => $request->input('author'),
            'description' => $request->input('description'),
            'keyword' => $request->input('keyword'),
            'robots' => $request->input('robots'),
            'canonical_url' => $request->input('canonical_url'),
            'language' => $request->input('language'),
            'generator' => "Avannubo",
            'route' => $request->input('url'),
            'og_title' => $request->input('og_title'),
            'og_description' => $request->input('og_description'),
            'og_type' => $request->input('og_type'),
            'og_image' => $og_imageName,
            'og_url' => $request->input('og_url'),
            'og_site_name' => $request->input('og_site_name'),
            'tw_card' => $request->input('tw_card'),
            'tw_url' => $request->input('tw_url'),
            'tw_title' => $request->input('tw_title'),
            'tw_description' => $request->input('tw_description'),
            'tw_image' => $tw_imageName,
            'gl_page_type' => $request->input('gl_page_type'),
            'gl_name' => $request->input('gl_name'),
            'gl_description' => $request->input('gl_description'),
            'gl_image' => $gl_imageName,
        ]);

        if($seos->save()){
            return redirect()->route('seos')->with(
                'message', 'Seo creado correctamente'
            );
        }else{
            return redirect()->route('seos')->with(
                'error', 'Seo no creado correctamente,intentelo mas tarde'
            );
        }

    }

    /**
     * @author: Obiang
     * @date: 04/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the seo in the database,
     * if it not existe redirect to index else show the update form
     */
    public function edit($id){
        $seos = Seo::find($id);
        if($seos){
            return view('seo::updateForm',compact('seos','id'));
        }
        return redirect()->route('seos')->with(
            'error', 'Seo no encontrado'
        );
    }

    /**
     * @author: Obiang
     * @date: 04/07/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the seo  in the database,
     * if it not existe redirect to index else and  update the information in the database
     */
    public function update(Request $request, $id){
        $seos = Seo::find($id);
        if($seos == null) {
            return redirect()->route('seos')->with(
                'error', 'Seo no encontrado'
            );
        }
        $rules = [
            'title' => 'nullable|Max:255',
            'author' => 'nullable|Max:255',
            'description' => 'nullable|Max:400',
            'keyword' => 'nullable|Max:255',
            'robots' => 'nullable|Max:255',
            'canonical_url' => 'nullable|url|Max:255',
            'language' => 'nullable|Max:255',
            'url' => 'required|url|Max:255|unique:seos,route,' . $id,
            'og_title' => 'nullable|Max:255',
            'og_description' => 'nullable|Max:400',
            'og_type' => 'nullable|Max:255',
            'og_image' => 'nullable|image',
            'og_url' => 'nullable|url|Max:255',
            'og_site_name' => 'nullable|Max:255',
            'tw_card' => 'nullable|Max:255',
            'tw_url' => 'nullable|url|Max:255',
            'tw_title' => 'nullable|Max:255',
            'tw_description' => 'nullable|Max:400',
            'tw_image' => 'nullable|image',
            'gl_page_type' => 'nullable|Max:255',
            'gl_name' => 'nullable|Max:255',
            'gl_description' => 'nullable|Max:400',
            'gl_image' => 'nullable|image',
        ];
        $this->validate($request, $rules);
        
        // control validate image
        if($request->file('og_image') != null){
            if ($seos->og_image) {
                if (Storage::disk('public')->exists('/seos/' . $seos->og_image)) {
                    Storage::disk('public')->delete('/seos/' . $seos->og_image);
                }
            }
            $og_image = $request->file('og_image');
            $og_imageName = time(). '.' .$og_image->getClientOriginalExtension();
            Storage::putFileAs('public/seos', new File($og_image), $og_imageName );
            $seos->og_image = $og_imageName;
        }else{
            $seos->og_image = $request->input('og_image');
        }

        if($request->file('tw_image') != null){
            if ($seos->tw_image) {
                if (Storage::disk('public')->exists('/seos/' . $seos->tw_image)) {
                    Storage::disk('public')->delete('/seos/' . $seos->tw_image);
                }
            }
            $tw_image = $request->file('tw_image');
            $tw_imageName = time(). '.' .$tw_image->getClientOriginalExtension();
            Storage::putFileAs('public/seos', new File($tw_image), $tw_imageName );
            $seos->tw_image = $tw_imageName;
        }else{
            $seos->tw_image = $request->input('tw_image');
        }

        if($request->file('gl_image') != null){
            if ($seos->gl_image) {
                if (Storage::disk('public')->exists('/seos/' . $seos->gl_image)) {
                    Storage::disk('public')->delete('/seos/' . $seos->gl_image);
                }
            }
            $gl_image = $request->file('gl_image');
            $gl_imageName = time(). '.' .$gl_image->getClientOriginalExtension();
            Storage::putFileAs('public/seos', new File($gl_image), $gl_imageName );
            $seos->gl_image = $gl_imageName;
        }else{
            $seos->gl_image = $request->input('gl_image');
        }

        $seos->title = $request->input('title');
        $seos->author = $request->input('author');
        $seos->description = $request->input('description');
        $seos->keyword = $request->input('keyword');
        $seos->robots = $request->input('robots');
        $seos->canonical_url = $request->input('canonical_url');
        $seos->language = $request->input('language');
        $seos->generator = "Avannubo";
        $seos->route = $request->input('url');
        $seos->og_title = $request->input('og_title');
        $seos->og_description = $request->input('og_description');
        $seos->og_type = $request->input('og_type');
        $seos->og_url = $request->input('og_url');
        $seos->og_site_name = $request->input('og_site_name');
        $seos->tw_card = $request->input('tw_card');
        $seos->tw_url = $request->input('tw_url');
        $seos->tw_title = $request->input('tw_title');
        $seos->tw_description = $request->input('tw_description');
        $seos->gl_page_type = $request->input('gl_page_type');
        $seos->gl_name = $request->input('gl_name');
        $seos->gl_description = $request->input('gl_description');

        $seos->update();
        if($seos){
            return redirect()->route('seos')->with(
                'message', 'Seo actualizado correctamente'
            );
        }else{
            return redirect()->route('seos')->with(
                'error', 'Seo no actualizado correctamente,intentelo mas tarde'
            );
        }
    }

    /**
    * @author: Obiang
    * @date: 04/07/2017
    * @param Request $request
    * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    * @description This method validates the values entered by the client, if they are correct it delete it into the database and
    * file local else Shows error
    */
    public function delete ($id){
        // Validation
        $seos = Seo::find($id);
        if($seos){
            if($seos->delete()) {
                if ($seos->og_image){
                    if (Storage::disk('public')->exists('/seos/' . $seos->og_image)) {
                        Storage::disk('public')->delete('/seos/' . $seos->og_image);
                    }
                }

                if ($seos->tw_image) {
                    if (Storage::disk('public')->exists('/seos/' . $seos->tw_image)) {
                        Storage::disk('public')->delete('/seos/' . $seos->tw_image);
                    }
                }

                if ($seos->gl_image){
                    if (Storage::disk('public')->exists('/seos/' . $seos->gl_image)) {
                        Storage::disk('public')->delete('/seos/' . $seos->gl_image);
                    }
                }

                Session::flash('message','Seo eliminado correctamente');
            }else{
                Session::flash('error','Seo no eliminado correctamente, intentelo mas tarde');
            }

        }
        else{
            Session::flash('error','Seo no encontrado');
        }
        return redirect()->route('seos');
    }



}

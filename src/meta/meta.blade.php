@if($seo_metas)
    @foreach($seo_metas  as $meta)
   <meta name="title" content="{{$meta->title or ''}}">
   <meta name="description" content="{{$meta->description or ''}}">
   <meta name="url" content="{{$meta->url or ''}}">
   <meta name="keyword" content="{{$meta->keyword or ''}}">
   <meta name="robots" content="{{$meta->robots or ''}}">
   <meta name="canonical" content="{{$meta->canonical_url or ''}}">
   <meta name="author" content="{{$meta->author or ''}}">
   <meta name="languange" content="{{$meta->language or ''}}">
   <meta name="generator" content="{{$meta->generator or ''}}">
   <meta name="route" content="{{$meta->route or ''}}">
   <meta property="og:title" content="{{$meta->og_title or ''}}">
   <meta property="og:description" content="{{$meta->og_description or ''}}">
   <meta property="og:type" content="{{$meta->og_type}}">
   <meta property="og:image" content="{{ asset('storage/seos/'.$meta->og_image) }}">
   <meta property="og:url" content="{{$meta->og_url or ''}}">
   <meta property="og:site_name" content="{{$meta->og_site_name or ''}}">
   <meta name="twitter:card" content="{{$meta->tw_card or ''}}">
   <meta name="twitter:url" content="{{$meta->tw_url or ''}}">
   <meta name="twitter:title" content="{{$meta->tw_title or ''}}">
   <meta name="twitter:description" content="{{$meta->tw_description or ''}}">
   <meta name="twitter:image" content="{{ asset('storage/seos/'.$meta->tw_image) }}">
   <meta itemprop="name" content="{{$meta->gl_name}}">
   <meta itemprop="description" content="{{$meta->gl_description}}">
   <meta itemprop="image" content="{{ asset('storage/seos/'.$meta->gl_image) }}">
   <meta itemprop="page_type" content="{{$meta->gl_page_type or '' }}">
   @endforeach
@endif


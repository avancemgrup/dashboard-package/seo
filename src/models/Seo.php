<?php

namespace Avannubo\Seo\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model{
    protected $fillable = [
        'id',
        'title',
        'author',
        'description',
        'keyword',
        'robots',
        'canonical_url'  ,
        'language',
        'generator',
        'route',
        'og_title',
        'og_description',
        'og_type',
        'og_image',
        'og_url',
        'og_site_name',
        'tw_card',
        'tw_url',
        'tw_title',
        'tw_description',
        'tw_image',
        'og_site_name',
        'gl_page_type',
        'gl_name',
        'gl_description',
        'gl_image',

];
}

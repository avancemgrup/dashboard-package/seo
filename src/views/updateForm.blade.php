@extends('layouts.administration.master')

@section('site-title')
    Seo
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Seo</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('seos') }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => ['seos-edit', $id], 'method'=>'PUT', 'enctype' => 'multipart/form-data')) !!}
                <div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
                    <label for="author">Autor</label>
                    {!! Form::text('author', $seos->author, array('placeholder' => 'Autor','class' => 'form-control','id' => 'author')) !!}
                    <span class="text-danger">{{ $errors->first('author') }}</span>
                </div>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="title">Título</label>
                    {!! Form::text('title', $seos->title, array('placeholder' => 'Título','class' => 'form-control','id' => 'title')) !!}
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="url">Url</label>
                    {!! Form::url('url', $seos->route, array('placeholder' => 'Url','class' => 'form-control','id' => 'url')) !!}
                    <span class="text-danger">{{ $errors->first('url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::textarea('description', $seos->description, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('v') ? 'has-error' : '' }}">
                    <label for="keyword">Keyword</label>
                    {!! Form::text('keyword', $seos->keyword, array('placeholder' => 'Keyword','class' => 'form-control','id' => 'keyword')) !!}
                    <span class="text-danger">{{ $errors->first('keyword') }}</span>
                </div>
                <div class="form-group {{ $errors->has('robots') ? 'has-error' : '' }}">
                    <label for="robots">Robots</label>
                    <select name="robots" class="form-control" id="sel1">
                        @if($seos->robots == "INDEX")
                            <option value="INDEX" selected>INDEX</option>
                        @else
                            <option value="INDEX">INDEX</option>
                        @endif
                        @if($seos->robots == "NOINDEX")
                            <option value="NOINDEX" selected>NOINDEX</option>
                        @else
                            <option value="NOINDEX" >NOINDEX</option>
                        @endif
                        @if($seos->robots == "FOLLOW")
                            <option value="FOLLOW" selected>FOLLOW</option>
                        @else
                            <option value="FOLLOW" >FOLLOW</option>
                        @endif
                        @if($seos->robots == "NOFOLLOW")
                            <option value="NOFOLLOW" selected>NOFOLLOW</option>
                        @else
                            <option value="NOFOLLOW" >NOFOLLOW</option>
                        @endif
                        @if($seos->robots == "INDEX,FOLOOW")
                            <option value="INDEX,FOLOOW" selected>INDEX AND FOLOOW</option>
                        @else
                            <option value="INDEX,FOLOOW" >INDEX AND FOLOOW</option>
                        @endif
                        @if($seos->robots == "INDEX,NOFOLLOW")
                            <option value="INDEX,NOFOLLOW" selected>INDEX AND NOFOLLOW</option>
                        @else
                            <option value="INDEX-NOFOLLOW" >INDEX AND NOFOLLOW</option>
                        @endif
                        @if($seos->robots == "NOINDEX,FOLLOW")
                            <option value="NOINDEX,FOLLOW" selected>NOINDEX AND FOLLOW</option>
                        @else
                            <option value="NOINDEX,FOLLOW" >NOINDEX AND FOLLOW</option>
                        @endif

                    </select>
                    <span class="text-danger">{{ $errors->first('robots') }}</span>
                </div>

                <div class="form-group {{ $errors->has('canonical_url') ? 'has-error' : '' }}">
                    <label for="canonical_url">Canonical URL</label>
                    {!! Form::url('canonical_url', $seos->canonical_url, array('placeholder' => 'Canonical URL','class' => 'form-control','id' => 'canonical_url')) !!}
                    <span class="text-danger">{{ $errors->first('canonical_url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label for="language">Idioma</label>
                    {!! Form::select('language', array('Spanish' => 'Spanish', 'Ingles' => 'Ingles', 'Catalan' => 'Catalan', 'Frances' => 'Frances'), $seos->language, ['class' => 'form-control', 'id' => 'language']) !!}
                    <span class="text-danger">{{ $errors->first('language') }}</span>
                </div>
                <h3>TWITER</h3>
                <div class="form-group {{ $errors->has('tw_card') ? 'has-error' : '' }}">
                    <label for="tw_card">Card</label>
                        <select name="tw_card" class="form-control" id="sel1">
                        @if($seos->tw_card == "summary")
                            <option value="summary" selected>Vista de tarjeta</option>
                        @else
                            <option value="summary">Vista de tarjeta</option>
                        @endif
                        @if($seos->tw_card == "summary_large_image")
                            <option value="summary_large_image" selected>Vista de tajeta con imagen grande</option>
                        @else
                            <option value="summary_large_image" >Vista de tajeta con imagen grande</option>
                        @endif
                        @if($seos->tw_card == "app")
                            <option value="app" selected>Vista de aplicación</option>
                        @else
                            <option value="app" >Vista de aplicación</option>
                        @endif
                        @if($seos->tw_card == "player")
                            <option value="player" selected>Vista de jugador</option>
                        @else
                            <option value="player" >Vista de jugador</option>
                        @endif
                    </select>
                    <span class="text-danger">{{ $errors->first('tw_card') }}</span>
                </div>
                <div class="form-group {{ $errors->has('tw_url') ? 'has-error' : '' }}">
                    <label for="tw_url">URL</label>
                    {!! Form::url('tw_url', $seos->tw_url, array('placeholder' => 'URL','class' => 'form-control','id' => 'tw_url')) !!}
                    <span class="text-danger">{{ $errors->first('url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="tw_title">Título</label>
                    {!! Form::text('tw_title', $seos->tw_title, array('placeholder' => 'Título','class' => 'form-control','id' => 'tw_title')) !!}
                    <span class="text-danger">{{ $errors->first('tw_title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('tw_description') ? 'has-error' : '' }}">
                    <label for="tw_description">Descripción</label>
                    {!! Form::textarea('tw_description', $seos->tw_description, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'tw_description')) !!}
                    <span class="text-danger">{{ $errors->first('tw_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('tw_image') ? 'has-error' : '' }}">
                    <label for="tw_image">Imagen</label>
                    <img src="{{ asset('storage/seos/' . $seos->tw_image) }}" alt="Image" style="max-width: 300px; max-height: 300px">
                    {!! Form::file('tw_image', array('accept' => 'image/*','class' => 'form-control','id' => 'tw_image')) !!}
                    <span class="text-danger">{{ $errors->first('tw_image') }}</span>
                </div>
                <h3>FACEBOOK</h3>
                <div class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }}">
                    <label for="og_title">Título</label>
                    {!! Form::text('og_title', $seos->og_title, array('placeholder' => 'Título','class' => 'form-control','id' => 'og_title')) !!}
                    <span class="text-danger">{{ $errors->first('og_title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_description') ? 'has-error' : '' }}">
                    <label for="og_description">Descripción</label>
                    {!! Form::textarea('og_description', $seos->og_description, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'og_description')) !!}
                    <span class="text-danger">{{ $errors->first('og_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('or_type') ? 'has-error' : '' }}">
                    <label for="og_type">Tipo</label>
                     <select name="og_type" class="form-control" id="sel1">
                        @if($seos->og_type == "books")
                            <option value="books" selected>Libros</option>
                        @else
                            <option value="books">Libros</option>
                        @endif
                        @if($seos->og_type == "article")
                            <option value="article" selected>Articulo</option>
                        @else
                            <option value="article" >Articulo</option>
                        @endif
                        @if($seos->og_type == "news")
                            <option value="news" selected>Noticia</option>
                        @else
                            <option value="news" >Noticia</option>
                        @endif
                        @if($seos->og_type == "business")
                            <option value="business" selected>Negocio</option>
                        @else
                            <option value="business" >Negocio</option>
                        @endif
                    </select>
                    <span class="text-danger">{{ $errors->first('og_type') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_image') ? 'has-error' : '' }}">
                    <label for="og_image">Imagen</label>
                    <img src="{{ asset('storage/seos/' . $seos->og_image) }}" alt="Image" style="max-width: 300px; max-height: 300px">
                    {!! Form::file('og_image', array('accept' => 'image/*','class' => 'form-control','id' => 'og_image')) !!}
                    <span class="text-danger">{{ $errors->first('og_image') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_url') ? 'has-error' : '' }}">
                    <label for="og_url">URL</label>
                    {!! Form::url('og_url', $seos->og_url, array('placeholder' => 'URL','class' => 'form-control','id' => 'og_url')) !!}
                    <span class="text-danger">{{ $errors->first('og_url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_site_name') ? 'has-error' : '' }}">
                    <label for="og_site_name">Nombre de web</label>
                    {!! Form::text('og_site_name', $seos->og_site_name, array('placeholder' => 'Nombre de web','class' => 'form-control','id' => 'og_site_name')) !!}
                    <span class="text-danger">{{ $errors->first('og_site_name') }}</span>
                </div>
                <h3>GOOGLE+</h3>
                <div class="form-group {{ $errors->has('gl_page_type') ? 'has-error' : '' }}">
                    <label for="gl_page_type">Tipo pagina</label>
                    <select name="gl_page_type" class="form-control" id="sel1">
                        @if($seos->gl_page_type == "video")
                            <option value="video" selected>Video</option>
                        @else
                            <option value="video">Video</option>
                        @endif
                        @if($seos->gl_page_type == "movie")
                            <option value="movie" selected>Película</option>
                        @else
                            <option value="movie" >Película</option>
                        @endif
                        @if($seos->gl_page_type == "article")
                            <option value="article" selected>Articulo</option>
                        @else
                            <option value="article" >Articulo</option>
                        @endif
                    </select>
                    <span class="text-danger">{{ $errors->first('gl_page_type') }}</span>
                </div>
                <div class="form-group {{ $errors->has('gl_name') ? 'has-error' : '' }}">
                    <label for="gl_name">Nombre</label>
                    {!! Form::text('gl_name', $seos->gl_name, array('placeholder' => 'Nombre','class' => 'form-control','id' => 'gl_name')) !!}
                    <span class="text-danger">{{ $errors->first('gl_name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('gl_description') ? 'has-error' : '' }}">
                    <label for="gl_description">Descripción</label>
                    {!! Form::textarea('gl_description', $seos->gl_description, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'gl_description')) !!}
                    <span class="text-danger">{{ $errors->first('gl_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('gl_image') ? 'has-error' : '' }}">
                    <label for="gl_image">Image</label>
                    <img src="{{ asset('storage/seos/' . $seos->gl_image) }}" alt="Image" style="max-width: 300px; max-height: 300px">
                    {!! Form::file('gl_image', array('accept' => 'image/*','class' => 'form-control','id' => 'gl_image')) !!}
                    <span class="text-danger">{{ $errors->first('gl_image') }}</span>
                </div>
                <button type="submit" class="btn btn-success">
                   Actualizar
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
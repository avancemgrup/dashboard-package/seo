@extends('layouts.administration.master')

@section('site-title')
    Seo
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Crear nuevo Seo</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('seos') }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'seos-add', 'method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
                <div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
                    <label for="author">Autor</label>
                    {!! Form::text('author', null, array('placeholder' => 'Autor','class' => 'form-control','id' => 'author')) !!}
                    <span class="text-danger">{{ $errors->first('author') }}</span>
                </div>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="title">Título</label>
                    {!! Form::text('title', null, array('placeholder' => 'Título','class' => 'form-control','id' => 'title')) !!}
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('route') ? 'has-error' : '' }}">
                    <label for="route">Url</label>
                    {!! Form::url('url', null, array('placeholder' => 'route','class' => 'form-control','id' => 'route')) !!}
                    <span class="text-danger">{{ $errors->first('route') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('v') ? 'has-error' : '' }}">
                    <label for="keyword">Keyword</label>
                    {!! Form::text('keyword', null, array('placeholder' => 'Keyword','class' => 'form-control','id' => 'keyword')) !!}
                    <span class="text-danger">{{ $errors->first('keyword') }}</span>
                </div>
                <div class="form-group {{ $errors->has('robots') ? 'has-error' : '' }}">
                    <label for="robots">Robots</label>
                    {!!Form::select('robots', array('INDEX'=>'INDEX','NOINDEX'=>'NOINDEX','FOLLOW'=>'FOLLOW','INDEX,FOLOOW'=>'INDEX AND FOLLOW','NOFOLLOW'=>'NOFOLLOW','INDEX,NOFOLLOW'=>'INDEX AND NOFOLLOW','NOINDEX,FOLLOW'=>'NOINDEX AND FOLLOW'),null,array('class' => 'form-control'))!!}
                    <span class="text-danger">{{ $errors->first('robots') }}</span>
                </div>
                <div class="form-group {{ $errors->has('canonical_url') ? 'has-error' : '' }}">
                    <label for="canonical_url">Canonical URL</label>
                    {!! Form::url('canonical_url', null, array('placeholder' => 'Canonical URL','class' => 'form-control','id' => 'canonical_url')) !!}
                    <span class="text-danger">{{ $errors->first('canonical_url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label for="language">Idioma</label>
                    {!! Form::select('language', array('Spanish' => 'Spanish', 'Ingles' => 'Ingles', 'Catalan' => 'Catalan', 'Frances' => 'Frances'), null, ['class' => 'form-control', 'id' => 'language']) !!}
                    <span class="text-danger">{{ $errors->first('language') }}</span>
                </div>
                <h3>Twitter </h3>
                <div class="form-group {{ $errors->has('tw_card') ? 'has-error' : '' }}">
                    <label for="tw_card">Card</label>
                        {!!Form::select('tw_card', array('summary'=>'Vista de tarjeta','summary_large_image'=>'Vista de tajeta con imagen grande','app'=>'Vista de aplicación','player'=>'Vista de jugador'),null,array('class' => 'form-control'))!!}
                    <span class="text-danger">{{ $errors->first('tw_card') }}</span>
                </div>
                <div class="form-group {{ $errors->has('tw_url') ? 'has-error' : '' }}">
                    <label for="tw_url">URL</label>
                    {!! Form::url('tw_url', null, array('placeholder' => 'URL','class' => 'form-control','id' => 'tw_url')) !!}
                    <span class="text-danger">{{ $errors->first('url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="tw_title">Título</label>
                    {!! Form::text('tw_title', null, array('placeholder' => 'Título','class' => 'form-control','id' => 'tw_title')) !!}
                    <span class="text-danger">{{ $errors->first('tw_title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('tw_description') ? 'has-error' : '' }}">
                    <label for="tw_description">Descripción</label>
                    {!! Form::textarea('tw_description', null, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'tw_description')) !!}
                    <span class="text-danger">{{ $errors->first('tw_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('tw_image') ? 'has-error' : '' }}">
                    <label for="tw_image">Imagen</label>
                    {!! Form::file('tw_image', array('accept' => 'image/*','class' => 'form-control','id' => 'tw_image')) !!}
                    <span class="text-danger">{{ $errors->first('tw_image') }}</span>
                </div>
                <h3>Facebook</h3>
                <div class="form-group {{ $errors->has('og_title') ? 'has-error' : '' }}">
                    <label for="og_title">Título</label>
                    {!! Form::text('og_title', null, array('placeholder' => 'Título','class' => 'form-control','id' => 'og_title')) !!}
                    <span class="text-danger">{{ $errors->first('og_title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_description') ? 'has-error' : '' }}">
                    <label for="og_description">Descripción</label>
                    {!! Form::textarea('og_description', null, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'og_description')) !!}
                    <span class="text-danger">{{ $errors->first('og_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('or_type') ? 'has-error' : '' }}">
                    <label for="og_type">Tipo</label>
                    {!!Form::select('og_type', array('books'=>'Libros','article'=>'Articulo','news'=>'Noticias','business'=>'Negocios'),null,array('class' => 'form-control'))!!}
                    <span class="text-danger">{{ $errors->first('og_type') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_image') ? 'has-error' : '' }}">
                    <label for="og_image">Imagen</label>
                    {!! Form::file('og_image', array('accept' => 'image/*','class' => 'form-control','id' => 'og_image')) !!}
                    <span class="text-danger">{{ $errors->first('og_image') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_url') ? 'has-error' : '' }}">
                    <label for="og_url">URL</label>
                    {!! Form::url('og_url', null, array('placeholder' => 'URL','class' => 'form-control','id' => 'og_url')) !!}
                    <span class="text-danger">{{ $errors->first('og_url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('og_site_name') ? 'has-error' : '' }}">
                    <label for="og_site_name">Nombre de web</label>
                    {!! Form::text('og_site_name', null, array('placeholder' => 'Nombre de web','class' => 'form-control','id' => 'og_site_name')) !!}
                    <span class="text-danger">{{ $errors->first('og_site_name') }}</span>
                </div>
                <h3>Google+</h3>
                <div class="form-group {{ $errors->has('gl_page_type') ? 'has-error' : '' }}">
                    <label for="gl_page_type">Tipo pagina</label>
                        {!!Form::select('gl_page_type', array('video'=>'Video','movie'=>'Pelicula','article'=>'Articulo'),null,array('class' => 'form-control'))!!}

                    <span class="text-danger">{{ $errors->first('gl_page_type') }}</span>
                </div>
                <div class="form-group {{ $errors->has('gl_name') ? 'has-error' : '' }}">
                    <label for="gl_name">Nombre:</label>
                    {!! Form::text('gl_name', null, array('placeholder' => 'Nombre','class' => 'form-control','id' => 'gl_name')) !!}
                    <span class="text-danger">{{ $errors->first('gl_name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('gl_description') ? 'has-error' : '' }}">
                    <label for="gl_description">Descripción</label>
                    {!! Form::textarea('gl_description', null, array('placeholder' => 'Descripción','class' => 'form-control','id' => 'gl_description')) !!}
                    <span class="text-danger">{{ $errors->first('gl_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('gl_image') ? 'has-error' : '' }}">
                    <label for="gl_image">Imagen</label>
                    {!! Form::file('gl_image', array('accept' => 'image/*','class' => 'form-control','id' => 'gl_image')) !!}
                    <span class="text-danger">{{ $errors->first('gl_image') }}</span>
                </div>
                <button type="submit" class="btn btn-success">
                   Crear
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@extends('layouts.administration.master')

@section('site-title')
    Seo
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('seo.create')
                        <a href="{{ route('seos-add-form') }}" class="btn btn-success">
                            Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-offset-6 col-lg-offset-6 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                {!! Form::open(array('route' => ['seos'], 'method'=>'get')) !!}
                                     {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Seo</h3>
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if($seos)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Descripción</th>
                                <th>Ruta</th>
                                @if(Entrust::can('seo.edit') || Entrust::can('seo.delete'))
                                <th>Opciones</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($seos as $seo)
                                <tr>
                                    <td>{{$seo->title}}</td>
                                    <td>{{$seo->description}}</td>
                                    <td>{{$seo->route}}</td>
                                    <td>
                                        @permission('seo.edit')
                                        <a href="{{ url('administration/seos/edit/'.$seo->id) }}" class="btn btn-default btn-icon">
                                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                        </a>
                                        @endpermission
                                        @permission('seo.delete')
                                        {!! Form::open(array('route' => ['seos-delete', $seo->id], 'method'=>'DELETE', 'enctype' => 'multipart/form-data', 'style' => 'display:inline-block')) !!}
                                        <button class="btn btn-danger btn-icon"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}
                                        @endpermission
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                    </div>
                    <div class="row middle-xs end-md end-lg">
                        {{ $seos->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
<?php

namespace Avannubo\Seo;


use Avannubo\Seo\Models\Seo;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class SeoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/Seo.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'seo');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        //publish meta
        $this->publishes([
            __DIR__.'/meta' => resource_path('views/meta')
        ], 'avannubo');

        try {
            $url = $this->app->request->fullUrl();
            $seo = Seo::where('route', $url)->get();
            View::share('seo_metas', $seo);
        } catch (\Exception $e) {
            return [];
        }

    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //register controller
        $this->app->make('Avannubo\Seo\Controllers\SeoController');

    }
}

<?php
namespace Avannubo\Seo\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionSeoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'seo.view',
            'display_name' => 'seo  view',
            'description' => 'Ver seo'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'seo.create',
            'display_name' => 'seo create',
            'description' => 'Crear seo'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'seo.edit',
            'display_name' => 'seo edit',
            'description' => 'Editar seo'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'seo.delete',
            'display_name' => 'seo delete',
            'description' => 'Eliminar seo'
        ]);
        $person->save();;
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeosTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('seos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('description')->nullable();
            $table->string('keyword')->nullable();
            $table->string('robots')->nullable();
            $table->string('canonical_url')->nullable();
            $table->string('language')->nullable();
            $table->string('generator')->nullable();
            $table->string('route');
            $table->string('og_title')->nullable();
            $table->string('og_description')->nullable();
            $table->string('og_type')->nullable();
            $table->string('og_image')->nullable();
            $table->string('og_url')->nullable();
            $table->string('og_site_name')->nullable();
            $table->string('tw_card')->nullable();
            $table->string('tw_url')->nullable();
            $table->string('tw_title')->nullable();
            $table->string('tw_description')->nullable();
            $table->string('tw_image')->nullable();
            $table->string('gl_name')->nullable();
            $table->string('gl_page_type')->nullable();
            $table->string('gl_description')->nullable();
            $table->string('gl_image')->nullable();

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('seos');
    }
}